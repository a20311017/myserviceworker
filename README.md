# MyFirstServiceWorker

David Edgardo García Salazar
IDGS 10-1

# Getting started

## Name
Simple First Service Worker

## Description
Service Worker to keep visited pages(sw_cached_site) or listed pages(sw_cached_pages) in cache 

## Authors and acknowledgment
Based on Traversy Media Intro to Service Workers https://www.youtube.com/watch?v=ksXwaWHCW6k

